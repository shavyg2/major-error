var Store=[];
var Extend = require("super-extend");


var e = function(c1,c2){
	Extend(c1,c2);
	exports[c1.name] = c1;
}



function BaseError(message) {
	this.superArgs(arguments);
	this.message = message;
	this.__proto__.constructor.prototype.name = this.__proto__.constructor.name;
	Error.captureStackTrace(this, arguments.callee);
}

BaseError.prototype.super=function(){
	Error.apply(this,arguments);
}

e(BaseError, Error);

//User Errors
function UserError() {
	this.superArgs(arguments)
}
e(UserError, BaseError);
//exports.UserError = UserError;

function InvalidField(){
	this.superArgs(arguments)
}
e(InvalidField,UserError);








function AccessError(message){
	this.superArgs(arguments);
	this.message = message || "Access Denied";
}

e(AccessError,BaseError);


function AuthenticationError(){
	this.superArgs(arguments)
}

e(AuthenticationError, AccessError);
//exports.AuthenticationError = AuthenticationError;



function NoAccountError(){
this.superArgs(arguments)
}

e(NoAccountError,AuthenticationError);





//Server Errors
function ServerError() {
	this.superArgs(arguments)
}
e(ServerError, BaseError);
//exports.ServerError = ServerError;

function IOError() {this.superArgs(arguments)}
e(IOError, ServerError);

function DatabaseError() {this.superArgs(arguments)}
e(DatabaseError, IOError);

function FileError() {
	this.superArgs(arguments)
}
e(FileError, IOError);

function UploadError(){
	this.superArgs(arguments);
}
e(UploadError,FileError);

Object.defineProperty(exports,"list",{
	enumberable:true,
	get:function(){
		var list =[];
		for(var key in exports){
			list.push(key);
		}
		return list;
	}
})



//Development Error
function DevelopmentError(message){
	this.super(message||"DevelopmentError has occurred");
}
e(DevelopmentError,BaseError);

function BadArgumentError(message){
	this.superArgs([message||"Missing or Bad Argument was used"]);
}
e(BadArgumentError,DevelopmentError);

function ImplementationError(message){
	this.super(message||"Functionality is not implemented");
}
e(ImplementationError,DevelopmentError);

function MethodNotImplementedError(message){
	this.super(message||"Method is not Implemented");
}
e(MethodNotImplementedError,ImplementationError);
